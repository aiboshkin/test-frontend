import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Authentication/Login.vue'
import Main from '../views/Main';
import NotFound from '../views/NotFound';

Vue.use(VueRouter);

const title = 'Test';

const routes = [
  {
    path: '/',
    name: 'Main',
    meta: {
      title: 'Главная',
    },
    component: Main,
  },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: 'Авторизация',
    },
    component: Login,
  },
  {
    path: '/404',
    name: '404',
    meta: {
      title: 'Not found',
    },
    component: NotFound,
  },
  {
    path: '*',
    redirect: '/404',
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});
router.afterEach((to) => {
  Vue.nextTick(() =>  {
    document.title = title + ' | ' + to.meta.title;
  });
});

router.beforeEach((to, from, next) => {
  const accessRoutes = ['Login'];
  if (!localStorage.getItem('token') && !accessRoutes.includes(to.name) ) {
    router.push({ name: 'Login' });
  } else next();
});

export default router;
