import { io } from 'socket.io-client';
import {
  GETTER_SOCKET_URL,
  SOCKET_EMIT_MESSAGE
} from '../const';

import store from '../store';

class SocketioService {
  socket;
  constructor() {}

  setupSocketConnection() {
    const url = store.getters[GETTER_SOCKET_URL];
    this.socket = io(url, {
      auth: {
        token: 'abc'
      }
    });
  }

  sendMessage(message) {
    this.socket.emit(SOCKET_EMIT_MESSAGE, {
      message
    });
  }

  disconnect() {
    if (this.socket) {
        this.socket.disconnect();
    }
  }
}

export default new SocketioService();