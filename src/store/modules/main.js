import {
  MUTATION_SET_ISLOADING,
  MUTATION_SET_URL
} from '../../const';
import socketioService from '../../services/socketio.service';

import jwt_decode from 'jwt-decode'

const state = {
  url: null
};

const getters = {
  socketUrl (state) {
    return state.url;
  }
};

const mutations = {
  setUrl (state, token) {
    const { url } = jwt_decode(token) || {};
    state.url = url;
  }
};

const actions = {
  connectSocket: ({ commit }, token) => {
    commit(MUTATION_SET_ISLOADING, true, { root: true });
    commit(MUTATION_SET_URL, token, { root: true });
    socketioService.setupSocketConnection();
    commit(MUTATION_SET_ISLOADING, false, { root: true });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
