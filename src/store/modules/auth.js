import {
  ACTION_ROOT_REQUEST,
  MUTATION_AUTH_LOGIN,
  MUTATION_SET_ISLOADING,
  URL_AUTH_LOGIN
} from '../../const';
import router from '@/router';

const state = {};

const getters = {};

const mutations = {
  login (state, response) {
    const token = response.token;
    if (token) {
      localStorage.setItem('token', token);
    }
  },

  logout (state) {
    if (state.url) {
      state.url = null;
      localStorage.removeItem('token');
    }
  }
};

const actions = {
  loginData: async ({ dispatch, commit }, { data }) => {
    commit(MUTATION_SET_ISLOADING, true, { root: true });
    await dispatch(ACTION_ROOT_REQUEST, {
      method: 'POST',
      url: URL_AUTH_LOGIN,
      data,
      commitName: MUTATION_AUTH_LOGIN,
    }, { root: true });
    commit(MUTATION_SET_ISLOADING, false, { root: true });
    router.push({ name: 'Main' });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
