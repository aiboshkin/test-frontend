//URL's
export const BASE_URL = 'http://localhost:3000';
export const URL_AUTH_LOGIN = '/users/signin';
export const URL_AUTH_SIGHOUT = 'users/signout';

//ACTIONS
export const ACTION_ROOT_REQUEST = 'http';

export const ACTION_AUTH_LOGIN = 'auth/loginData';
export const ACTION_MAIN_CONNECT_SOCKET = 'main/connectSocket';

//MUTATIONS
export const MUTATION_AUTH_LOGIN = 'auth/login';
export const MUTATION_AUTH_SIGNOUT = 'auth/logout';

export const MUTATION_SET_URL = 'main/setUrl';

export const MUTATION_SET_ISLOADING = 'setIsLoading';

//GETTERS
export const GETTER_SOCKET_URL = 'main/socketUrl';

export const GETTER_LOADING = 'loading';

//SOCKET

export const SOCKET_EMIT_MESSAGE = 'message';