import Vue from 'vue'
import App from './App.vue'
import { initElement } from './modules/Element_io';
import store from './store';
import router from './router';

initElement(Vue);

Vue.config.productionTip = false

export const VueInstance = new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
