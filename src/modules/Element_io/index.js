import {
  Card,
  Container,
  Input,
  Loading,
  Notification
} from 'element-ui';

export function initElement (Vue) {
  Vue.use(Card);
  Vue.use(Container);
  Vue.use(Input);
  Vue.use(Loading);
  
  Vue.prototype.$notify = Notification;
}
